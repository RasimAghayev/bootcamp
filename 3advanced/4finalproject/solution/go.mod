module golangdojo.com/golangdojo/bootcamp/3advanced/4finalproject/solution

go 1.18

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/lib/pq v1.10.7 // indirect
	golang.org/x/crypto v0.3.0 // indirect
)
